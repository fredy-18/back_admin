<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\CoursesController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth'], function () {   
    Route::post('me',  [AuthController::class ,'me'])->name('me');
    Route::post('login', [AuthController::class ,'authenticated']);
    Route::post('logout', [AuthController::class ,'logout']); 
    Route::post('password/email', [ForgotPasswordController::class,'forgot']);
    Route::post('password/reset', [ForgotPasswordController::class,'reset']);
});

Route::group(['prefix' => 'users','middleware' => ['auth:sanctum']], function () {   
    Route::middleware('checkAdmin')->get('get_students',  [UsersController::class ,'get_students'])->name('get_students');
    Route::middleware('checkAdmin')->post('save_user/{id?}',  [UsersController::class ,'save_user'])->name('save_user');
    Route::middleware('checkAdmin')->delete('delete_user/{id}',  [UsersController::class ,'delete_user'])->name('delete_user');
    Route::middleware('checkAdmin')->get('get_user/{id}',  [UsersController::class ,'get_user'])->name('get_user');
});
Route::group(['prefix' => 'courses','middleware' => ['auth:sanctum']], function () {   
    Route::middleware('checkAdmin')->get('get_courses',  [CoursesController::class ,'get_courses'])->name('get_courses');
    Route::middleware('checkAdmin')->post('save_course/{id?}',  [CoursesController::class ,'save_course'])->name('save_course');
    Route::middleware('checkAdmin')->delete('delete_course/{id}',  [CoursesController::class ,'delete_course'])->name('delete_course');
    Route::middleware('checkAdmin')->get('get_course/{id}',  [CoursesController::class ,'get_course'])->name('get_course');
    Route::middleware('checkAdmin')->delete('delete_course_student/{id}',  [CoursesController::class ,'delete_course_student'])->name('delete_course_student');
    Route::middleware('checkAdmin')->post('save_course_student',  [CoursesController::class ,'save_course_student'])->name('save_course_student');
    Route::middleware('checkAdmin')->get('get_students_course/{course_id}',  [CoursesController::class ,'get_students_course'])->name('get_students_course');
    Route::middleware('checkAdmin')->get('search_student',  [CoursesController::class ,'search_student'])->name('search_student');
    Route::get('get_student_courses',  [CoursesController::class ,'get_student_courses'])->name('get_student_courses');

    
   
});

