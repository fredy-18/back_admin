<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use Illuminate\Support\Facades\Auth;
use App\Models\CouserUser;
use DB;
class CoursesController extends Controller
{
    public function __construct()
    {        
    
    }
    
    public function get_courses(Request $req)
    {
        $courses = Course::where('deleted', "0")->get();
        return response()->json(["courses" =>  $courses],200);
    }
    public function save_course(Request $req, $id = -1)
    {
       
        $validate = [
            'name' => 'required|max:255' ,
            'intensity' => 'numeric|required'            
        ];
       
        $req->validate($validate);
        $data = [
            'name' => $req->name,
            'intensity' => $req->intensity,            
            "deleted" =>0
        ];

        if($id < 0)
            $course =  Course::create( $data);
        else {
            $course = Course::where("id",$id)->update( $data);
        }
        if($course){
            return response()->json(["success" =>true ,"message" => "Curso guardado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al guardar"],200);
        }
    }  

    function delete_course(Request $req, $id){
        $user = course::where("id",$id)->update(["deleted"=>1]);
        if($user){
            return response()->json(["success" =>true ,"message" => "Curso eliminado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al eliminar"],200);
        }
    }
    function get_course(Request $req, $id){
        $course = Course::where("deleted",0)->where("id",$id)->first();
        if($course){
            return response()->json(["success" =>true ,"course" => $course],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Curso no encontrado"],200);
        }
    }

    function get_students_course(Request $req, $course_id){
        $students = CouserUser::select(["u.name", "u.id as user_id","cousers_users.id"])
        ->join("users as u", "u.id","=","cousers_users.user_id")
        ->where('course_id', $course_id)->get();
        return response()->json(["students" =>  $students],200);
    }
    function save_course_student(Request $req){
        $validate = [
            'user_id' => 'numeric|required' ,
            'course_id' => 'numeric|required'            
        ];
       
        $req->validate($validate);
        $data = [
            'user_id' => $req->user_id,
            'course_id' => $req->course_id
        ];
        $exists = CouserUser::where('user_id', '=', $req->user_id)->where('course_id', '=', $req->course_id)->exists();
        $course = true;
        if(!$exists)
            $course =  CouserUser::create( $data);    
       
        if($course){
            return response()->json(["success" =>true ,"message" => "Datos guardado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al guardar"],200);
        }
    }
    function search_student(Request $req){
        
		$search = $req->search;
		$search = trim($search);
	
	   $suggestion = DB::table("users")->select(["name","id"])->where('rol',"student")->where('deleted',0)	  
       ->where(function($query) use ($search)
       {		 	
			$query->where('name', 'like', "%".$search."%")->orWhere('email', 'like', "%".$search."%");
		})->offset(0)->limit(15)->get();
    	return response()->json(["students" => $suggestion], 200);	
    }
    function delete_course_student(Request $req,$id){
        $user = CouserUser::where("id",$id)->delete();
        if($user){
            return response()->json(["success" =>true ,"message" => "Curso eliminado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al eliminar"],200);
        }
    }
    function get_student_courses(){
        $user_id = auth()->user()->id;
        $cousers = DB::table("courses as c")->select(["c.name", "c.intensity","c.id"])
        ->join("cousers_users as cu", "cu.course_id","=","c.id")
        ->where('cu.user_id', $user_id)->get();
        return response()->json(["cousers" =>  $cousers],200);

    }
   
    
}
