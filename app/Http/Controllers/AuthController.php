<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
class AuthController extends Controller
{
    public function __construct()
    {        
       $this->middleware('auth:sanctum', ['except' => ['authenticated']]); 
    }
    
    public function authenticated(Request $request)
    {
        $dataUser = array("success" => true, "message"=>"ok");
        $user =  Auth::guard('sanctum')->user();
      
        if(!$user){
            $data = $request->validate([
                'email' => 'required|email',
                'password' => 'required',
                'device_name' => 'required',
            ]);

            $user = User::where('email', $request->email)->where("deleted",0)->first();
            if($user){                
                if (! \Hash::check($request->password, $user->password)) {                   
                    $dataUser["success"] = false; 
                    $dataUser["message"] = "Contraseña incorrecta";                
                }
                else if($user) {
                    $dataUser["token"] = $user->createToken($request->device_name)->plainTextToken;
                    $dataUser["user"] = $user;
                }
            }else{
                $dataUser["success"] = false;        
                $dataUser["message"] = "Usuario no existe";
            }
        }
        else
            $dataUser["message"] = "Usuario logueado";       

       
        return response()->json($dataUser,200);
    }  
   
    public function me()
    {      
        $user = Auth::user(); 
        $data["user"] =  $user;      
        return response()->json($data);
    }   
   
    public function logout()
    {
       $user = Auth::user();
       $user->currentAccessToken()->delete();        
        return response()->json([
            'success' => true,
            'body'=>  [
                'message'=> 'Sesion finalizada'          
            ]
        ]);
    } 
}
