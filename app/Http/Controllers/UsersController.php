<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Hash;
class UsersController extends Controller
{
    public function __construct()
    {        
    
    }
    
    public function get_students(Request $req)
    {
        $users = User::where('rol', "student")->where('deleted', "0")->get();
        return response()->json(["users" =>  $users],200);
    }
    public function save_user(Request $req, $id = -1)
    {
       
        $validate = [
            'name' => 'required|max:255',
            'email'  => 'required|email|unique:users,email,'.$id       
        ];
        if($id == -1){
            $validate["email"]  = 'required|email|unique:users,email,'.$id;
            $validate["password"]  = 'required|max:255';
        }
        $req->validate($validate);
        $data = [
            'name' => $req->name,
            'email' => $req->email,
            'phone' =>empty( $req->phone) ? null: $req->phone ,            
            "rol" =>"student",
            "deleted" =>0
        ];
        
        if(!empty($req->password)){
            $data["password"] = Hash::make($req->password);
        }

        if($id < 0)
            $user =  User::create( $data);
        else {
            $user = User::where("id",$id)->update( $data);
        }
        if($user){
            return response()->json(["success" =>true ,"message" => "Usuario guardado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al guardar"],200);
        }
    }  

    function delete_user(Request $req, $id){
        $user = User::where("id",$id)->update(["deleted"=>1]);
        if($user){
            return response()->json(["success" =>true ,"message" => "Usuario eliminado"],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Erro al eliminar"],200);
        }
    }
    function get_user(Request $req, $id){
        $user = User::where("deleted",0)->where("id",$id)->first();
        if($user){
            return response()->json(["success" =>true ,"user" => $user],200);
        }else{
            return response()->json(["success" =>false ,"message" => "Usuario no encontrado"],200);
        }
    }
   
    
}
