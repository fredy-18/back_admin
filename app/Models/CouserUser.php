<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class CouserUser extends Model 
{
    protected $table  = 'cousers_users';

    protected $fillable = [
        'user_id',
        'course_id'
    ];
}
